package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText NombreUsuario;
    EditText ContraseñaUsuario;
    EditText Descripcion;
    EditText EdadUsuario;
    EditText NumeroUsuario;
    public int contador = 0;

    //Variables para Buscar
    String NombreB;
    String NumeroM;
    //login
    EditText NombreUsuarioLog;
    EditText ContraseñaUsuarioLog;

    Button BotonRegistrar;
    Button BotonLogin;
    public String [] NombreUsuarios = {"","","","","","","","","",""};
    public String [] ContraseñasUsuarios= {"","","","","","","","","",""};
    public String[] Descripciones= {"","","","","","","","","",""};
    public String[] Edades= {"","","","","","","","","",""};
    public String[] NumerosT= {"","","","","","","","","",""};
    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BotonRegistrar = (Button) findViewById(R.id.btnRegistrar);
        BotonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NombreUsuario = (EditText) findViewById(R.id.edtNombreUsuario1);
                ContraseñaUsuario = (EditText) findViewById(R.id.edtNuevaContraseña);
                Descripcion = (EditText) findViewById(R.id.edtDescripcion);
                EdadUsuario = (EditText) findViewById(R.id.edtEdad);
                NumeroUsuario = (EditText) findViewById(R.id.edtNumeroTelefonico);

                if(contador == 10){
                    Toast.makeText(getApplicationContext(),"Ya existen registros",Toast.LENGTH_LONG);
                }else{
                    for (int i = 0; i<=contador; i++ ){
                        NombreUsuarios[i] = NombreUsuario.getText().toString();
                        ContraseñasUsuarios[i] = ContraseñaUsuario.getText().toString();
                        Descripciones[i] = Descripcion.getText().toString();
                        Edades[i] = EdadUsuario.getText().toString();
                        NumerosT[i] = NumeroUsuario.getText().toString();
                    }
                    Toast.makeText(getApplicationContext(),"Se registró con éxito",Toast.LENGTH_LONG);
                    contador+=1;
                }

                

            }
        });


        BotonLogin = (Button) findViewById(R.id.btnIngresar);
        BotonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NombreUsuarioLog = (EditText) findViewById(R.id.edtNombreU);
                ContraseñaUsuarioLog = (EditText) findViewById(R.id.edtContraseña);
                if (NombreUsuarioLog.getText().toString().equals(NombreUsuarios[0]) && ContraseñaUsuarioLog.getText().toString().equals(ContraseñasUsuarios[0])){
                    Intent Ac2 = new Intent(getApplicationContext(), MainActivity2.class);
                    startActivity(Ac2);
                }else{
                    Toast.makeText(getApplicationContext(),"Contraseña Incorrecta",Toast.LENGTH_LONG);
                }
            }
        });
    }
    public void Guardar(View view){

        for (int i = 0; i<NombreUsuarios.length ; i++){
            NombreB = NombreUsuarios[i];
            NumeroM = NumerosT[i];
        }
        SharedPreferences preferencias = getSharedPreferences("agenda", MODE_PRIVATE);
        SharedPreferences.Editor obj_editor = preferencias.edit();
        obj_editor.commit();
    }

    public void Buscar(View view){
        for (int i = 0; i<NombreUsuarios.length ; i++){
            NombreB = NombreUsuarios[i];
            NumeroM = NumerosT[i];
        }
    }
}